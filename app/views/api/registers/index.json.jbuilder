json.array!(@registers) do |register|


	json.(register, :id, :what, :where, :start_date, :end_date, :price, :promotor, :promotor_contact, :latitude, :longitude, :description, :pictures_videos)

	json.category do |json|
		json.(register.category, :id, :name)
	end

            json.user do |json|
                json.(register.user, :id, :email, :name)
            end

end