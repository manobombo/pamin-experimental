class ApplicationController < ActionController::Base
  include Pundit
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :null_session

  acts_as_token_authentication_handler_for User, fallback_to_devise: false

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def user_not_authorized
      render nothing: true, status: :forbidden
  end

  def pundit_user
    current_api_user
  end

end
