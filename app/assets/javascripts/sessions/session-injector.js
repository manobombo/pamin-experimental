(function () {
    'use strict';

    angular
        .module('pamin.sessions')
        .factory('SessionInjector', SessionInjector);

    SessionInjector.$injector = ['Session'];
    function SessionInjector(Session) {
            var sessionInjector = {
                request: function(config) {
                    if (Session.isAuthenticated()) {
                        config.headers['X-User-Token'] = Session.getToken();
                        config.headers['X-User-Email'] = Session.getEmail();
                    }

                    return config;
                }
            };

        return sessionInjector;
    }

})();