(function () {
    'use strict';

    angular
        .module('pamin.sessions')
        .service('Auth', Auth);

    Auth.$injector = ['$rootScope', '$http', '$q', 'Session'];
    function Auth($rootScope, $http, $q, Session) {
        var auth = this;

        auth.login = login;
        auth.logout = logout;
        auth.isAuthenticated = isAuthenticated;
        auth.validateToken = validateToken;
        auth.current_user = current_user;

        function validateToken() {
            return $http.get('/api/auth/validate_token');
        }

        function login(credentials) {
            var user = {
                api_user: credentials
            };

            var deferred = $q.defer();
            var promise = deferred.promise;

            $http
                .post('/api/auth/sign_in', user)
                .then(function (res) {
                    var headers = res.data.authentication_headers;

                    Session.setEmail(headers.user_email);
                    Session.setToken(headers.user_token);

                    Session.setUser(res.data.user);

                    deferred.resolve();
                }, function (res) {
                    deferred.reject(res.data.errors);
                });

            return promise;
        }

        function logout() {
            var deferred = $q.defer();
            var promise = deferred.promise;

            $http
                .delete('/api/auth/sign_out')
                .then( function () {
                    Session.destroy();

                    deferred.resolve();
                }, function () {
                    deferred.reject();
                });

            return promise;
        }

        function current_user() {
            return Session.getUser();
        }

        function isAuthenticated() {
            return Session.isAuthenticated();
        }

    }
})();