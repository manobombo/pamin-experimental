(function () {
    'use strict';

    angular
        .module('pamin.registers')
        .controller('NewRegisterController', NewRegisterController);

    NewRegisterController.$injector = ['$scope', '$rootScope', 'Category', 'Register', 'PaminMap', '$location', 'Notify'];

    function NewRegisterController($scope, $rootScope, Category, Register, PaminMap, $location, Notify) {
        var vm = this;

        vm.add = add;
        vm.categories = Category.query();

        vm.register = new Register();

        $scope.$emit('refreshMap:main');

        $scope.$on('paminMap:click', onClickMap);
        $rootScope.$on('$routeChangeStart', onRouteChange);

        function add() {
            if(vm.register.category_id === undefined){
                Notify.alert("Escolha uma categoria.");
                return false;
            }

            if(vm.register.latitude === undefined) {
                Notify.alert("Escolha um ponto no mapa.");
                return false;
            }

            Register.save(vm.register, function () {
                Notify.alert('Cadastrado com sucesso!');
                $scope.$emit('refreshMap:main');
                PaminMap.setDefaultZoom();
                $location.path('/')
            });
        }

        function onClickMap(event, coords) {
            vm.register.latitude = coords.lat;
            vm.register.longitude = coords.lng;

            PaminMap.addMarker('tmp:newregister', {
                position: coords
            });
        }

        function onRouteChange(scope, next, current) {
            try {
                if (current !== next)
                    PaminMap.removeMarker('tmp:newregister');
            } catch(err) { console.debug('error'); }
        }
    }
})();