( function () {
    'use strict';

    angular
        .module('pamin.filters')
        .filter('byCategory', byCategory);

    function byCategory() {
        return function (markers, category) {
            angular.forEach(markers, function (marker) {
                var visible = marker.getData().category == category

                marker.getReference().setVisible(visible);
            });

            return markers;
        }
    }
})();