( function () {
    'use strict';

    angular
        .module('pamin.users')
        .factory('User', User);

    User.$injector = ['$resource'];

    function User($resource) {
        return $resource('/api/users/:id', {
            update: { method: 'PUT' }
        });
    }
})();