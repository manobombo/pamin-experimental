class UserRegisterRelationship < ActiveRecord::Migration
  change_table :registers do |t|
    t.belongs_to :user
  end
end
