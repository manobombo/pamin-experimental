class AddLatitudeAndLongitudeToRegisters < ActiveRecord::Migration
  def change
  	change_table :registers do |t|
  		t.decimal :latitude
  		t.decimal :longitude
  	end
  end
end
