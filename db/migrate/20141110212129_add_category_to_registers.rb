class AddCategoryToRegisters < ActiveRecord::Migration
  def change
  	change_table :registers do |t|
  		t.references :category
  	end
  end
end
