class CreateRegisters < ActiveRecord::Migration
  def change
    create_table :registers do |t|
      t.string :what
      t.string :where
      t.datetime :start_date
      t.datetime :end_date
      t.decimal :price
      t.string :promotor
      t.string :promotor_contact

      t.timestamps
    end
  end
end
