class AddDescriptionToRegisters < ActiveRecord::Migration
  def change
  	change_table :registers do |t|
  		t.text :description
  	end
  end
end
