class AddArrayLinksToRegisters < ActiveRecord::Migration
  def change
  	add_column :registers, :pictures_videos, :string, array: true,  default: '{}'
  end
end
