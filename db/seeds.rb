# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

	User.delete_all
	admin = User.create({
		name: 'Administrador',
		email: 'admin@pamin.lavid.ufpb.br',
		#birthday: Time.new(2012, 01, 01, 0, 0, 0, '-03:00'),
		password: '123mudar',
		password_confirmation: '123mudar'
	})


	Category.delete_all
	Category.create([
		{name: 'Pessoas'},
		{name: 'Lugares'},
		{name: 'Celebrações'},
		{name: 'Saberes'},
		{name: 'Formas de Expressão'},
		{name: 'Objetos'}
	])

	Register.delete_all
	Register.create({
		user: admin,
		what: 'Parque Sólon de Lucena',
		where: 'Centro, João Pessoa',
		start_date: Time.new(1939, 01, 1, 0, 0, 0, "-03:00"),
		price: 0,
		promotor: 'Prefeitura de João Pessoa',
		promotor_contact: '+55 83 3218-9000',
		description: 'Também conhecida como Lagoa, localizada no Centro de João Pessoa - PB.',
		latitude: -7.1201795,
		longitude: -34.8797785,
		category: Category.find_by(name: 'Lugares')
	})